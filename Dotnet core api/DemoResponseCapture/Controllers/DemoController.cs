﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Data.SqlClient;
using Dapper;

namespace DemoResponseCapture.Controllers
{
   
    [ApiController]
    [Route("[controller]")]
    public class DemoResponseController : ControllerBase
    {

        private readonly ILogger<DemoResponseController> _logger;

        public DemoResponseController(ILogger<DemoResponseController> logger)
        {
            _logger = logger;
        }

        [HttpPost]
        
        public async Task<FunctionResponse> updateResponse([FromBody] CellObj DemoValue)
        {
            string connectionString = "Data Source='ServerName';Initial Catalog='DBNAME';UID='sa'; PWD='PWD'";
            try
            {
                if (DemoValue.MerchantInvoice == null || DemoValue.MerchantInvoice == "")
                {
                    return new FunctionResponse { status = "Error", result = "MerchantInvoice required!" };
                }
                if (DemoValue.DemoResponseList.Count == 0)
                {
                    return new FunctionResponse { status = "Error", result = "response is empty!" };
                }

                using (SqlConnection  con = new SqlConnection(connectionString))
                {
                    string queryMain = @"insert into MAIN(MerchantName,MerchantBank,MerchantAccount,MerchantInvoice) values
                    (@MerchantName,@MerchantBank,@MerchantAccount,@MerchantInvoice)";
                    con.Execute(queryMain, DemoValue);
                    string query = @$"INSERT INTO STATUS (INVOICE,NAME,BCODE,ACCOUNT,AMOUNT,DESCRIPTION,
PAYMENTSTATUS,ID,FEE) 
                    values 
                    (@invoice, @Name,@BCode,@Account,@Amount,@Description,@PaymentStatus,@Id,@Fee)";
                    int res = con.Execute(query, DemoValue.DemoResponseList.ToList());
                    if (res == 0)
                    {
                        return new FunctionResponse { status = "error", result = res };
                    }
                    else
                    {
                        return new FunctionResponse { status = "ok", result = res };
                    }
                }
         
                //return new FunctionResponse { status = "Sucess", result = DemoValue };
            }
            catch (Exception ex)
            {

                return new FunctionResponse { status = "Error", result = ex };
            }
        }
    }
}
public class FunctionResponse
{
    public string status { get; set; }
    public object result { get; set; }
    public Exception Ex { get; set; }
    public string message { get; set; }
    

}

public class FunctionResponseEventArgs : EventArgs
{
    public FunctionResponse Response;
}

public class CellObj
{
    [Required(ErrorMessage = "MerchantName required")]
    public string MerchantName { get; set; }

    [Required(ErrorMessage = "MerchantBank required")]
    public string MerchantBank { get; set; }

    [Required(ErrorMessage = "MerchantAccount required")]
    public string MerchantAccount { get; set; }

    [Required(ErrorMessage = "MerchantInvoice required")]
    public string MerchantInvoice { get; set; }
    
    
    public List<DemoResponse> DemoResponseList { get; set; }
}
public class DemoResponse
{
    [Required(ErrorMessage = "invoice required")]
    public string invoice { get; set; }
    [Required(ErrorMessage = "Name required")]
    public string Name { get; set; }
    [Required(ErrorMessage = "BCode required")]
    public string BCode { get; set; }
    [Required(ErrorMessage = "Account required")]
    public string Account { get; set; }
    [Required(ErrorMessage = "Amount required")]
    public string Amount { get; set; }
    [Required(ErrorMessage = "Fee is required")]
    public string Fee { get; set; }
    [Required(ErrorMessage = "Description required")]
    public string Description { get; set; }
    [Required(ErrorMessage = "PaymentStatus required")]
    public string PaymentStatus {get;set;}
    [Required(ErrorMessage = "DemoId")]
    public string DemoId { get; set; }
}
   
